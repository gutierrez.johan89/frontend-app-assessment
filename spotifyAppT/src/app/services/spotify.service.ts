import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators'

@Injectable()
export class SpotifyService {

  constructor(private http: HttpClient) {}

  getQuery( query: string ) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDukCBHL9YsKDmDDj_SKFC-DrE2F0tZ7fTezrd9W2pJGbJfG9FgDqOnXbtNlpFQTjWrGmRvJ2LO6A4_2D8'
    });

    return this.http.get(url, { headers });
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases').pipe(map( data => data['albums'].items));
  }

  getArtists(id: string) {
    return this.getQuery(`search?q=${id}&type=artist&limit=15`).pipe(map( data => data['artists'].items));
  }

  getArtist( id: string ) {
    return this.getQuery(`artists/${id}`);
  }

  getArtistAlbums(id: string) {
    return this.getQuery(`artists/${id}/albums`).pipe(map( data => data['items'] ));
  }

  getAlbumTracks(id: string) {
    return this.getQuery(`albums/${id}/tracks`).pipe(map( data => data['items'] ));
  }
}
