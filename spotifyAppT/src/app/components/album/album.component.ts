import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  tracks: any[] = [];
  artistId: string = '';
  constructor(
    private router: ActivatedRoute,
    private SpotifyService: SpotifyService
    ) {
      this.router.params.subscribe(params => {
        this.getAlbumTracks(params['id']);
      });
     }

  ngOnInit(): void {
  }

  getAlbumTracks( id: string) {
    this.SpotifyService.getAlbumTracks(id).subscribe( tracks => {
      this.artistId = tracks[0].artists[0].id;
      this.tracks = tracks;
    });
  }
}
