import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  artists: any[] = [];
  errorData: any = {};
  error: boolean = false;
  loading: boolean = false;

  constructor(private spotifyService: SpotifyService) { }

  ngOnInit(): void {
  }

  search(term: string) {
    this.loading = true;
    setTimeout(()=>{
      this.spotifyService.getArtists(term).subscribe((artists: any) => {
        this.artists = artists;
        this.loading = false;
        this.error = false;
      }, (errorServicio) => {
        this.error = true;
        this.errorData = {
          message: errorServicio.error.error.message,
          status: errorServicio.error.error.status
        };
      });
   }, 700);
  }

}
