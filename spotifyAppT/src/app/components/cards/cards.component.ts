import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  @Input() items: any[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getItemList(item: any) {
    let artistId;
    let albumId;
    if (item.type === 'artist') {
      artistId = item.id;
      this.router.navigate([ '/artist', artistId ]);
    } else {
      albumId = item.id;
      this.router.navigate([ '/album', albumId ]);
    }
  }
}
