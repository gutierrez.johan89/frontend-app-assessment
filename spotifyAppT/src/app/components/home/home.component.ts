import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  newReleases: any[] = [];
  loading: boolean = false;
  errorData: any = {};
  error: boolean = false;

  constructor( private spotifyService: SpotifyService ) {
    this.loading = true;

  }

  ngOnInit(): void {
    setTimeout(()=>{
      this.spotifyService.getNewReleases().subscribe( (newReleases: any) => {
        this.newReleases = newReleases;
        this.loading = false;
      }, (errorServicio) => {
        this.error = true;
        this.loading = false;
        this.errorData = {
          message: errorServicio.error.error.message,
          status: errorServicio.error.error.status
        };
      });
    }, 700);
  }
}
