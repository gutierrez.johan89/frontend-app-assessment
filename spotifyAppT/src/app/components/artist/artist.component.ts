import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
  albums: any[] = [];
  artist: any = {};

  errorData: any = {};
  error: boolean = false;

  constructor(
    private router: ActivatedRoute,
    private SpotifyService: SpotifyService) {

    this.router.params.subscribe(params => {
      this.getArtistAlbums(params['id']);
      this.getArtist(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getArtistAlbums( id: string) {
    this.SpotifyService.getArtistAlbums(id).subscribe( albums => {
      this.albums = albums;
    }, (errorServicio) => {
      this.error = true;
      this.errorData = {
        message: errorServicio.error.error.message,
        status: errorServicio.error.error.status
      };
    });
  }

  getArtist( id: string ) {
    this.SpotifyService.getArtist(id).subscribe( artist => {
      this.artist = artist;
    }, (errorServicio) => {
      this.error = true;
      this.errorData = {
        message: errorServicio.error.error.message,
        status: errorServicio.error.error.status
      };
    });
  }

}
