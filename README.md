## How to run the project?

Clone project, move to **spotifyAppT** folder , then run npm install 

run on postman Post request
https://accounts.spotify.com/api/token 

body key values 

**grant_type:** client_credentials

**client_id:** a4b337bc368d45f682ecddec51e7c683

**client_secret:** ba1977b3ee8442d0abb8203dfe8c74b8

or use your own credentials from spotify

to get a token 

{
    "access_token": "BQAUz_G7hN7Oe420dIN4XMWKwWXG9WdaB5cRUB-wJ8Qy-sIkhvw6mNShdWxeve1FtQxJvtLPQZpysYVKX_Y",
    "token_type": "Bearer",
    "expires_in": 3600
}

then at spotify.service.ts replace in headers - Authorization the token.

const headers = new HttpHeaders({
    'Authorization': 'Bearer **ADD_GENERATED_TOKEN**'
});
